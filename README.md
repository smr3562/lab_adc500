# Lab ADC 500 Repo

## Before building
This lab requieres two external repositories, one is the Comblock and the other is the ADC500 driver. You can clone both manually, however both are added in this project as submodules. To initialize them you can use the following commands:

```
$ git clone https://gitlab.com/smr3562/lab_adc500.git
$ cd lab_adc500
$ git submodule update --init --recursive
```


## Autobuild

### From linux Terminal

Setup Vivado enviroment. 
```
$ source <path_to_vivado>/Vivado/2019.1/settings64.sh
$ vivado -s lab_adc500.tcl
```

### From Vivado Terminal
1. From **Tcl Console** cd to **../smr3562/LAB_ADC500 and run:
   ```
   source lab_adc500.tcl
   ```

#### From Sources 
1. Create a new vivado project for Zedboard 
3. Add *ip_repo* repository to the IP location.
4. From "Add sources/Add or create design sources" menu, add  *ip_repo/adc500* directory to the sources  
5. From "Add sources/Add or create constraints" menu, add "const/Zedboard" directory
6. Change directory to "LAB_ADC500" in the TCL folder
   ```
   cd ../smr385/LAB_ADC500/
   ```
7. Generate the block diagram by running the following command:
   ```
   source bd/Top.tcl
   ```
8. Create Wrapper and set it as top. 
9. Generate bitstream.

### More information
[smr3562 Lab ADC500](https://gitlab.com/smr3562/labs/-/wikis/lab_adc500)